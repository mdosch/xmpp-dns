# Changelog

## UNRELEASED

## [0.4.5] 2024-12-11
### Changed
- Host Meta (2): Improve output format.
- Host Meta (2): Suppress error if we don't get a valid http response.
- Host Meta (2): Suppress output when seeing non-XMPP link rels as defined in https://www.iana.org/assignments/link-relations/link-relations.xhtml.

## [0.4.4] 2024-12-08
### Changed
- Host Meta (2): Suppress error if no http server is available as we can't assume every xmpp server also operates an http server.
- Host Meta (2): Improve output format.

## [0.4.3] 2024-10-14
### Changed
- Host Meta (2): Improve output readability.
- Host Meta (2): Explicitly allow redirects.
- Host Meta 2: Also check host-meta2 if host-meta failed.
- QUIC: Improve layout.
- Websocket S2S: Set from attribute.
- Direct TLS: Stop connection test after failure.

## [0.4.2] 2024-08-25
### Added
- Host Meta 2: Show and test c2s Direct TLS.
- Host Meta 2: Show and test s2s Direct TLS.
- Host Meta 2: Show c2s Quic.
- Host Meta 2: Show s2s Quic.
- Host Meta 2: Show and test s2s Websocket.

## [0.4.1] 2024-08-20
### Added
- Also check connection to BOSH endpoints.
- Add more verbose version information to `--version` (requires xmppsrv >= 0.3.2).

### Changed
- Also respect custom resolver setting for host-meta(2) (via xmppsrv 0.3.1).

## [0.4.0] 2024-08-18
### Added
- Support for XEP-0487 (Host Meta 2).
- Support for XEP-0156 (Discovering Alternative XMPP Connection Methods).
- Support for connecting over websockets.

### Changed
- Only check standard ports as fallback.
- Simplify output.

## [0.3.11] 2024-04-07
### Changed
- Properly detect all stream errors.

## [0.3.10] 2023-12-25
### Changed
- Fix stream initialization bug with StartTLS (thanks moparisthebest).
- Further improve StartTLS stream initialization.
- Improve error messages when server closes connection.

## [0.3.9] 2023-11-09
### Changed
- Also apply timeout when server doesn't reply to StartTLS initialisation.

## [0.3.8] 2023-08-22
### Changed
- Don't look up IPs and don't try to connect if the SRV records target is ".".

## [0.3.7] 2023-08-15
### Added
- Show reason for denied s2s due to policy-violation.

### Changed
- Apply timeout to TLS handshake as well instead of only for the tcp connection.

## [0.3.6] 2023-03-27
### Changed
- Remove `println()` added for debugging.

## [0.3.5] 2023-03-27
### Changed
- Don't use CNAME of xmpp server domain for SRV lookups (via xmppsrv 0.2.5).

## [0.3.4]
### Changed
- Fix namespace for xmpps-client stream opening (`jabber:client` instead of `jabber:server`).
- Add `from` attribute for server-to-server connection test.

## [0.3.3]
### Changed
- Fix wrong timeout parsing on some systems.

## [0.3.2]
### Changed
- Fix detection of XMPP support when using direct TLS and ALPN.

## [0.3.1]
### Changed
- Print error details if SRV lookup fails.
- Print error details if IP lookup fails.
- Respect CNAME records.
- Detect CNAME loops (max. 5 CNAMEs) (via xmppsrv >= 0.2.4)

## [0.3.0]
### Added
- Possibility to specify the DNS resolver.
- DoT support

### Changed
- [golint] Coding style improvements.

## [0.2.4]
### Added
- Provide manpages.

## Changed
- Don't sort SRV records with the same priority by weight (via xmppsrv >= 0.1.1)

## [0.2.3]
### Added
- Make connection timeout configurable.

## [0.2.2]
### Added
- Possibility to test fallback ports if no SRV records are provided.
### Changed
- Disable colored output in windows.

## [0.2.1]
### Changed
- Change tlsConfig.NextProtos instead of appending.

## [0.2.0]
### Added
- Support for xmpps-server SRV records

## [0.1.0]
### Added
- Initial release
