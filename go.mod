module salsa.debian.org/mdosch/xmpp-dns

go 1.23.0

toolchain go1.24.0

require (
	github.com/pborman/getopt/v2 v2.1.0
	golang.org/x/net v0.37.0
	salsa.debian.org/mdosch/xmppsrv v0.3.3
)
