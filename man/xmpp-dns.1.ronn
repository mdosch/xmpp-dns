xmpp-dns(1) -- A CLI tool to check XMPP SRV records.
====

## SYNOPSIS

`xmpp-dns` [-46cfstv] [--help] [--no-color] [--timeout value] [--tls-version value] [--version] [parameters ...]

## DESCRIPTION

A little CLI tool to check SRV records for XMPP. Beside showing the records it can also
try to connect to the server and also test StartTLS and direct TLS.

## OPTIONS

* `-4`:
Resolve IPv4.

* `-6`:
Resolve IPv6.

* `-c`, `--client`:
Show client SRV records.

* `--dot`:
Use DoT, see also `--resolver`.

* `-f`, `--fallback`:
Check fallback (Standard ports on A/AAAA records) if no SRV records are provided.

* `--help`:
Show help.

* `--no-color`:
Don't colorize output.

* `--resolver`:
Custom resolver e.g. "1.1.1.1" for common DNS or "5.1.66.255#dot.ffmuc.net" for usage with "--dot".

* `-s`, `--server`:
Show server SRV records.

* `-t`:
Test connection and certificates.

* `--timeout`=[<value>]:
Connection timeout in seconds. Default: 60.

* `--tls-version`=[<value>]:
Minimal TLS version. 10 (TSLv1.0), 11 (TLSv1.1), 12 (TLSv1.2) or 13 (TLSv1.3).
Default: 12.

* `-v`:
Resolve IPs.

* `--version`:
Show version information.

## SHELL COMPLETION

### ZSH

There are no shell completions yet (contributions welcome) but for zsh it is
possible to automatically create completions from `--help` which might work
good enough.

Just place the following in your `~/.zshrc` or `~/.zshrc.local`:

```
compdef _gnu_generic xmpp-dns
```

### FISH

There are no shell completions yet, but FISH can generate them from the man page with following command:

```
fish_update_completions
```

## CHAT

There is no dedicated chat for `xmpp-dns`, but feel free to join
[https://join.jabber.network/#go-sendxmpp@chat.mdosch.de?join](https://join.jabber.network/#go-sendxmpp@chat.mdosch.de?join).

## AUTHOR

Written by Martin Dosch.

## REPORTING BUGS

Report bugs at [https://salsa.debian.org/mdosch/xmpp-dns/issues](https://salsa.debian.org/mdosch/xmpp-dns/issues).

## COPYRIGHT

Copyright (c) Martin Dosch
License: BSD-2-clause License
